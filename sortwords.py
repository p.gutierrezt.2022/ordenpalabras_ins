#!/usr/bin/env pythoh3

'''
Program to order a list of words given as arguments
'''

import sys


def is_lower(first: str, second: str):
    """Return True if first is lower (alphabetically) than second

    Order is checked after lowercasing the letters
    `<` is only used on single characteres.
    """
    list = []
    lower = False
    for pos in range(len(first)):
        list = []
        if pos < len(second):
            list.append(pos)
            if first[pos].lower() < second[pos].lower():
                lower = True
                break
            else:
                break
    return lower

def sort_pivot(words: list, pos: int):
    """Sorts word in pivot position, moving it to the left until its place"""

    lower: int = pos
    for value in range(pos, len(words)):
        if is_lower(words[value], words[lower]):
            value, lower = lower, value
    return lower

def sort(words: list):
    """Return the list of words, ordered alphabetically"""

    for pivot in range(len(words)):
        lower = sort_pivot(words, pivot)
        if lower != pivot:
            words[lower], words[pivot] = words[pivot], words[lower]
    return words

def show(words: list):
    """Show words on screen, using print()"""

    for word in words:
        print(word, end=' ')
    print()


def main():
    words: list = sys.argv[1:]
    sort(words)
    show(words)

if __name__ == '__main__':
    main()

